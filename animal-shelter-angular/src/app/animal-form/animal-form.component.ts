import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';
import { Owner } from '../models/owner.model';
import { OwnerService } from '../providers/owner.service';
import {formatDate } from '@angular/common';
@Component({
  selector: 'app-animal-form',
  templateUrl: './animal-form.component.html',
  styleUrls: ['./animal-form.component.css']
})
export class AnimalFormComponent {
  title: string = 'Add Animal';
  animal!: Animal;
  animalId!: string;
  showModalBool: boolean = false;
  displayStyle:string = "none";
  modalTitle: string = "Add an Animal";
  modalBody: string = "Thank you for registering your pet!";
  animalName!: string;
  animalBreed!: string;
  animalSpecies!: string;
  ownerName: string = '';
  dob!: string;
  checkInDate!: string;
  checkOutDate!: string;
  owner!: Owner;
  action!: string;

  constructor(private animalService: AnimalService, private ownerService: OwnerService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          debugger;
          if( params['id'] != undefined){
            this.animalId = params['id'];
            this.title = 'Update Animal';
            this.modalTitle = 'Update an Animal';
            this.modalBody = 'Animal data has been updated!';
            this.action = "PUT";
          } else {
            this.title = 'Add Animal';
            this.modalTitle = 'Add an Animal';
            this.modalBody = 'Thank you for registering your pet!';
            this.action = "POST";
          }

        }
      );

      this.animalService.getAnimalById(this.animalId).subscribe(data => {
        debugger;
        this.animal = data;
        if (this.animal != undefined){
          this.animalName = this.animal.name;
          this.animalBreed = this.animal.breed;
          this.animalSpecies = this.animal.species;
          if (this.animal.owner != undefined) {
            this.owner = this.animal.owner;
            this.ownerName = this.animal.owner.name;
          }
          this.dob = formatDate(this.animal.dob, 'yyyy-MM-dd', 'en-US');
          this.checkInDate = formatDate(this.animal.checkInDate, 'yyyy-MM-dd', 'en-US');
          this.checkOutDate = formatDate(this.animal.checkOutDate, 'yyyy-MM-dd', 'en-US');
        }
        debugger;
      });
  }

  openPopup() {
    this.displayStyle = "block";
  }

  onClose(): void {
    this.displayStyle = "none";
  }

  onSubmitForm() {
    debugger;
    if (this.ownerName != undefined && this.owner == undefined) {
      this.ownerService.getOwnerByName(this.ownerName).subscribe(data => {
        this.owner = data;
        this.action == 'PUT' ? this.sendPutRequest() : this.sendPostRequest();
      },
      error => {
        this.showModalBool = false;
        console.log("Error getting owner name: " + error.status); // the HTTP status code
        console.log(error.error); // the message from the API
        });
    }else{
      this.action == 'PUT' ? this.sendPutRequest() : this.sendPostRequest();
    }
    }

    sendPostRequest() {
      debugger;
      this.animal = new Animal(this.animalName, this.animalSpecies, this.animalBreed, this.owner.ownerId,this.dob,this.checkInDate,this.checkOutDate, this.owner, '');
      const params = Object.assign({}, this.animal);
      delete params.owner;
      delete params.animalId;

      this.animalService.createAnimal(params).subscribe(data => {
      this.openPopup();
      this.showModalBool = true;
    },
    error => {
      this.showModalBool = false;
      console.log(error.status); // the HTTP status code
      console.log(error.status); // the status code message
      console.log(error.error); // the message from the API
      });

    }

    sendPutRequest() {
      debugger;
      this.animal = new Animal(this.animalName, this.animalSpecies, this.animalBreed, this.owner.ownerId,this.dob,this.checkInDate,this.checkOutDate, this.owner, this.animalId);
      const params = Object.assign({}, this.animal);
      delete params.owner;

      this.animalService.updateAnimal(params, this.animalId).subscribe(data => {
      this.openPopup();
      this.showModalBool = true;
    },
    error => {
      this.showModalBool = false;
      console.log(error.status); // the HTTP status code
      console.log(error.status); // the status code message
      console.log(error.error); // the message from the API
      });

    }
}
