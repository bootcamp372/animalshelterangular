import { Component } from '@angular/core';
import { AnimalService } from '../providers/animal.service';
import { Owner } from '../models/owner.model';
import { OwnerService } from '../providers/owner.service';

import { ConfirmationDialogService } from '../providers/confirmation-dialog.service';
@Component({
  selector: 'app-owners',
  templateUrl: './owners.component.html',
  styleUrls: ['./owners.component.css']
})
export class OwnersComponent {
  title: string = "Owners Table";
  owners!: Array<Owner>;
  ownerAnimals!: Array<any>;
  deleteData!: any;
  showOwnerAnimalsBool: boolean = false;
  showAnimalsBtnName: string = 'Show Animals';

  constructor(private ownerService: OwnerService, private confirmationDialogService: ConfirmationDialogService) {
  }

  ngOnInit(): void {
    this.ownerService.getOwners().subscribe(data => {
      this.owners = data;
      if (this.owners != undefined){
        this.owners.forEach(owner => {

        });
        // this.ownerAnimals = this.owners.animals;
      }
    });
  }

  deleteOwner(id: string): void {
    this.ownerService.deleteOwner(id).subscribe(data => {
      this.deleteData = data;
      alert("Owner with ID " + id + " has been deleted.");
      window.location.reload();
    });
  }

  openConfirmationDialog(id: string) {
      if(confirm("Are you sure you want to delete owner with ID " + id + "?")) {
        console.log("User confirmed deletion");
        this.deleteOwner(id);
    }
  }

  toggleAnimalTable(animals: Array<any>) {
    debugger;
    this.ownerAnimals = animals;
    this.showOwnerAnimalsBool = !this.showOwnerAnimalsBool;

    if(this.showOwnerAnimalsBool)
        this.showAnimalsBtnName = "Hide Animals";
      else
        this.showAnimalsBtnName = "Show Animals";
    }
  }



