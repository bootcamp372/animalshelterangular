import { Owner } from "./owner.model";
export class Animal {
  constructor(

    public name: string,
    public species: string,
    public breed: string,
    public ownerId: string,
    public dob: string,
    public checkInDate: string,
    public checkOutDate: string,
    public owner?: Owner,
    public animalId?: string,
  ) { }
}
