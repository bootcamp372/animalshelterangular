import { Animal } from "./animal.model";

export class Owner {
  constructor(

    public name: string,
    public address1: string,
    public address2: string,
    public city: number,
    public state: string,
    public zipcode: string,
    public email: string,
    public phone: string,
    public ownerId: string,
    public animals: Animal[]

  ) { }
}
