import { Component } from '@angular/core';
import { AnimalService } from '../providers/animal.service';

import { ConfirmationDialogService } from '../providers/confirmation-dialog.service';
@Component({
  selector: 'app-animals',
  templateUrl: './animals.component.html',
  styleUrls: ['./animals.component.css']
})
export class AnimalsComponent {
  title: string = "Animals Table";
  animals!: Array<any>;
  deleteData!: any;
  toggleModalBool: boolean = false;

  constructor(private animalsService: AnimalService, private confirmationDialogService: ConfirmationDialogService) {
  }

  ngOnInit(): void {
    this.animalsService.getAnimals().subscribe(data => {
      this.animals = data;
    });
  }

  deleteAnimal(id: string): void {
    this.animalsService.deleteAnimal(id).subscribe(data => {
      this.deleteData = data;
      alert("Animal with ID " + id + " has been deleted.");
      window.location.reload();
    });
  }

  openConfirmationDialog(id: string) {
      if(confirm("Are you sure you want to delete animal with ID " + id + "?")) {
        console.log("User confirmed deletion");
        this.deleteAnimal(id);
    }
  }

  // toggleModal(): void {
  //   debugger;
  //   this.toggleModalBool == true ? this.toggleModalBool = false  : this.toggleModalBool = true ;
  // }

  // public openConfirmationDialog(id: string) {
  //   this.confirmationDialogService.confirm('Please confirm..', 'Do you really want to delete this record?')
  //   .then((confirmed: any) => {console.log('User confirmed:', confirmed); this.deleteAnimal(id)})
  //   .catch(() => console.log('User dismissed the dialog'));
  // }

}
