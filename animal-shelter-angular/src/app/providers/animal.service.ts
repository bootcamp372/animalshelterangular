import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Animal } from '../models/animal.model';

@Injectable({
  providedIn: 'root'
})
export class AnimalService {

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private animalURL: string =
  'https://localhost:7251/api/animals';
  // 'http://localhost:8081/api/animals';

  getAnimals(): Observable<Animal[]> {
    return this.http.get(this.animalURL,
      this.httpOptions).pipe(map(res => <Animal[]>res));
  }

  getAnimalById(id: string): Observable<Animal> {
    return this.http.get(this.animalURL + '/' + id,
      this.httpOptions).pipe(map(res => <Animal>res));
  }

  createAnimal(animal: Animal): Observable<Animal> {
    return this.http.post<Animal>(this.animalURL, animal, this.httpOptions)
          .pipe(map(res => <Animal>res));
  }

  updateAnimal(animal: Animal, id: string): Observable<Animal> {
    return this.http.put<Animal>(this.animalURL + '/' + id, animal, this.httpOptions)
          .pipe(map(res => <Animal>res));
  }

  deleteAnimal(id: string): Observable<Animal> {
    return this.http.delete(this.animalURL + '/' + id,
      this.httpOptions).pipe(map(res => <Animal>res));
  }

}
