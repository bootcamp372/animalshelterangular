import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject, of} from 'rxjs';
import { map } from 'rxjs/operators';
import { Owner } from '../models/owner.model';

@Injectable({
  providedIn: 'root'
})
export class OwnerService {

  constructor(private http: HttpClient) { }

  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json'
    })
  };

  private ownerURL: string =
  'https://localhost:7251/api/Owners';
  // 'http://localhost:8081/api/animals';

  getOwners(): Observable<Owner[]> {
    return this.http.get(this.ownerURL,
      this.httpOptions).pipe(map(res => <Owner[]>res));
  }

  getOwnerById(id: string): Observable<Owner> {
    return this.http.get(this.ownerURL + '/' + id,
      this.httpOptions).pipe(map(res => <Owner>res));
  }

  getOwnerByName(name: string): Observable<Owner> {
    // https://localhost:7251/api/Owners/owner-by-name/?name=Jon Arbuckle
    return this.http.get<Owner>(this.ownerURL + '/owner-by-name/?name=' + name, this.httpOptions)
          .pipe(map(res => <Owner>res));
  }

  createOwner(owner: Owner): Observable<Owner> {
    return this.http.post<Owner>(this.ownerURL, owner, this.httpOptions)
          .pipe(map(res => <Owner>res));
  }

  updateOwner(owner: Owner): Observable<Owner> {
    return this.http.put<Owner>(this.ownerURL, owner, this.httpOptions)
          .pipe(map(res => <Owner>res));
  }

  deleteOwner(id: string): Observable<Owner> {
    return this.http.delete(this.ownerURL + '/' + id,
      this.httpOptions).pipe(map(res => <Owner>res));
  }
}
