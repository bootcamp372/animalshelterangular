import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AnimalsComponent } from './animals/animals.component';
import { OwnersComponent } from './owners/owners.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FooterComponent } from './footer/footer.component';

import { AnimalService } from './providers/animal.service';
import { AnimalDetailsComponent } from './animal-details/animal-details.component';
import { AnimalFormComponent } from './animal-form/animal-form.component';
import { ConfirmationDialogComponent } from './confirmation-dialog/confirmation-dialog.component';
import { OwnerDetailsComponent } from './owner-details/owner-details.component';

const appRoutes: Routes = [
  { path: "", component: HomeComponent },
  { path: "home", component: HomeComponent },
  { path: "animals", component: AnimalsComponent },
  { path: "animal-details/:id", component: AnimalDetailsComponent },
  { path: "animal-form", component: AnimalFormComponent },
  { path: "animal-form/:id", component: AnimalFormComponent },
  { path: "owners", component: OwnersComponent }
  // { path: "owner-detail/:id", component: OwnerDetailsComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AnimalsComponent,
    OwnersComponent,
    NavBarComponent,
    FooterComponent,
    AnimalDetailsComponent,
    AnimalFormComponent,
    ConfirmationDialogComponent,
    OwnerDetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [AnimalService],
  bootstrap: [AppComponent]
})
export class AppModule { }
