import { Component, EventEmitter, Input, Output } from '@angular/core';



@Component({
  selector: 'app-confirmation-dialog',
  templateUrl: './confirmation-dialog.component.html',
  styleUrls: ['./confirmation-dialog.component.css']
})
export class ConfirmationDialogComponent {

  displayStyle!: string;
  @Input() title!: string;
  @Input() body!:string;

  @Output() close: EventEmitter<void> = new EventEmitter<void>();

  onModalClose(): void {
    this.close.emit();
  }

}


