import { Component } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Animal } from '../models/animal.model';
import { AnimalService } from '../providers/animal.service';

import {formatDate } from '@angular/common';

@Component({
  selector: 'app-animal-details',
  templateUrl: './animal-details.component.html',
  styleUrls: ['./animal-details.component.css']
})

export class AnimalDetailsComponent {
  animal!: any;
  animalId!: string;
  title: string = "Animal Details for ";

  constructor(private animalService: AnimalService, private route: ActivatedRoute) {
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.animalId = params['id'];
        }
      );

      this.animalService.getAnimalById(this.animalId).subscribe(data => {
        this.animal = data;
        this.animal.dob = formatDate(this.animal.dob, 'yyyy-MM-dd', 'en-US');
        this.animal.checkInDate = formatDate(this.animal.checkInDate, 'yyyy-MM-dd', 'en-US');
        this.animal.checkOutDate = formatDate(this.animal.checkOutDate, 'yyyy-MM-dd', 'en-US');
      });
  }

}
